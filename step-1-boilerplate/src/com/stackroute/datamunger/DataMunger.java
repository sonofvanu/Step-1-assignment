package com.stackroute.datamunger;

import java.util.*;

public class DataMunger {

	public static void main(String[] args) {
		// read the query from the user into queryString variable
		Scanner scanner=new Scanner(System.in);
		String queryString=scanner.nextLine();
		DataMunger dataMunger=new DataMunger();
		dataMunger.parseQuery(queryString);
		// call the parseQuery method and pass the queryString variable as a parameter
	}
	

	public void parseQuery(String queryString) {
		//call the methods
		getSplitStrings(queryString);
		getFile(queryString);
		getBaseQuery(queryString);
		getConditionsPartQuery(queryString);
		getConditions(queryString);
		getLogicalOperators(queryString);
		getFields(queryString);
		getOrderByFields(queryString);
		getGroupByFields(queryString);
		getAggregateFunctions(queryString);
	}
	
	// parse the queryString into words and display
	public String[] getSplitStrings(String queryString) {
		
		String[] splittedStrings=queryString.split(" ");
		System.out.println("The Splitted Query:.......");
		for(String eachStringOfQuery:splittedStrings)
		{
			System.out.println(eachStringOfQuery);
		}
		return splittedStrings;
	}

	// get and display the filename
	public String getFile(String queryString) {
		
		String fileName=(queryString.split("from")[1].trim()).split("where")[0].trim();
		System.out.println("FileName of the csv....."+fileName);
		return fileName;
	}
	
	// getting the baseQuery and display
	public String getBaseQuery(String queryString) {
		String baseQuery="";
		if(queryString.contains("where"))
		{
		baseQuery=(queryString.split("where")[0].trim());
		}
		else if(queryString.contains("group by")||queryString.contains("order by"))
		{
			baseQuery=(queryString.split("order by|group by")[0].trim());	
		}
		else
		{
			baseQuery=queryString;
		}
		System.out.println("The BaseQuery from the query...."+baseQuery);
		return baseQuery;
	}
	
	// get and display the where conditions part(if where condition exists)
	public String getConditionsPartQuery(String queryString) {
		
		if(queryString.contains("where"))
		{
		String conditionsQuery=(queryString.split("where")[1].trim()).split("order by|group by")[0].trim().toLowerCase();
		System.out.println("Conditional Query Part...."+conditionsQuery);
		return conditionsQuery;
		}
		else
		{
		return null;
		}

	}
	
	/* parse the where conditions and display the propertyName, propertyValue and
	 conditionalOperator for each conditions*/
	public String[] getConditions(String queryString) {
		if(queryString.contains("where"))
		{
		String[] conditions=(((queryString.split("where")[1].trim()).split("order by|group by")[0].trim().toLowerCase()).split(" and | or "));
		System.out.println("Each where conditions.......");
		for(String eachCondition:conditions)
		{
			System.out.println("propertyName......."+eachCondition.split(">=|<=|!=|<|>|=")[0].trim()+"       conditionalOperator........"+eachCondition.split(eachCondition.split(">=|<=|!=|<|>|=")[1].trim())[0].trim().split(eachCondition.split(">=|<=|!=|<|>|=")[0].trim())[1].trim()+"         propertyValue........"+eachCondition.split(">=|<=|!=|<|>|=")[1].trim());
		}
		return conditions;
		}
		else
		{
			return null;	
		}
		
	}
	
	// get the logical operators(applicable only if multiple conditions exist)
	public String[] getLogicalOperators(String queryString) {
		StringBuffer stringBuffer=new StringBuffer();
		if(queryString.contains("where"))
		{
		String[] conditionsQuery=((queryString.split("where")[1].trim()).split("order by|group by")[0].trim().toLowerCase()).split(" ");
		String[] logicalOperators=new String[getConditions(queryString).length-1];
		int index=0;
		for(String eachCondition:conditionsQuery)
		{
			if(eachCondition.equals("and")||eachCondition.equals("or"))
			{
				logicalOperators[index]=eachCondition;
				index++;
			}
			
		}
		System.out.println("The logical operators are.......");
		for(String eachLogicalOperator:logicalOperators)
		{
			System.out.println(eachLogicalOperator);
		}
		return logicalOperators;
		}
		else
		{
			return null;
		}
		
	}
	
	/*get the fields from the select clause*/
	public String[] getFields(String queryString) {
		System.out.println("Fields from the query......");
		String[] fields=(queryString.split("from")[0].trim()).split("select")[1].trim().split(",");
		for(String eachField:fields)
		{
			System.out.println(eachField);
		}
		return fields;
		
	}
	// get order by fields if order by clause exists
	public String[] getOrderByFields(String queryString) {
		if(queryString.contains("order by"))
		{
			System.out.println("order by field.....");
		String[] orderByField=(queryString.split("order by")[1].trim()).split("group by")[0].trim().split(" ");
		for(String eachOrderByField:orderByField)
		{
			System.out.println(eachOrderByField);
		}
		return orderByField;
		}
		
		return null;
	}
	
	// get group by fields if group by clause exists
	public String[] getGroupByFields(String queryString) {
		if(queryString.contains("group by"))
		{
			System.out.println("group by field.....");
		String[] groupByFields=(queryString.split("group by")[1].trim()).split("order by")[0].trim().split(" ");
		for(String eachGroupByField:groupByFields)
		{
			System.out.println(eachGroupByField);
		}
		return groupByFields;
		}
		return null;
	}
	
	// parse and display aggregate functions(if applicable)
	public String[] getAggregateFunctions(String queryString) {
		if(queryString.contains("("))
		{
			String[] aggregateFunctions=new String[queryString.split("\\(",-1).length-1];
			int index=0;
		for(String eachField:getFields(queryString))
		{
			if(eachField.contains("(")&& eachField.contains(")"))
			{
				aggregateFunctions[index]=eachField;
				index++;
			}
		}
		
		System.out.println("The aggregate Functions......");
		for(String eachAggregateFunction:aggregateFunctions)
		{
			System.out.println(eachAggregateFunction);
			System.out.println("Aggregate Function......."+eachAggregateFunction.split("\\(")[0]+"       aggregate column........"+eachAggregateFunction.split("\\(")[1].trim().split("\\)")[0]);
		}
		return aggregateFunctions;
		}
		else
		{
		return null;
		}
	}

	
	
	
	
}